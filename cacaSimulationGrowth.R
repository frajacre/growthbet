#FA 12/04/2018


#This is a script to test if the different growth patterns observed between western and eastern PAcific
#can be explained, at least to an extent, by differences in fish vertical distribution.

#Issues to solve: I could reorder rows in dat to eliminate duplicates in caught (when the same hook catches several fish)
#When subsetting, I might also need to eliminate duplicates (change runif by a sample function)



#The population in each region
#I need to take the numbers at age from the latest stock assessment model.
packages=c('R4MFCL','fishmethods','mgcv','RNetCDF','date','maps')
sapply(packages,function(x) {if (!x %in% installed.packages()) install.packages(x)}) 
lapply(packages, require, character.only=T)


#Now I assume they have a similar growth, and will use the one given in the latest stock assessment
#I have to generate distritions for each age class.
rep=read.rep('./BET_2017_basecase/plot-12.par.rep') 
# rep=read.par('./bet-PO-2015/10.par')
# 
Lmean<-rep$mean.LatAge
Lsd<-rep$sd.LatAge

#The number of fish the terminal year in region 4 would be given by (I use the same age structure in all the regions
#because it can also cause a bias in the estimates)
Natage<-rep$NatYrAgeReg[256,,4]
sum(Natage)

#To work with something that can be handled, I reduce the population....
Natage<-round(Natage/100,0)

#Now, I will take all those fish and will assign them with a length given Lmean and Lsd...
dat<-data.frame(ages<-unlist(sapply(1:40,function(x){rep(x,times=Natage[x])})),lengths<-unlist(sapply(1:40,function(x){rnorm(n=Natage[x],mean=Lmean[x],sd=Lsd[x])})))
names(dat)<-c('age','length')


#############################################
#Predict the depth of the fish
#############################################
model<-get(load('md_fx.Rdata'))

#Load functions to estimate depth of 20 and 18 deg isotherm, a
source('utils.r')
depth20<-depthattemp(2007,3,20)
depth18<-depthattemp(2007,3,18)

#Image on depth of thermocline.
plotimage(depth20$lon,depth20$lat,depth20$val,main='Depth of the thermocline (m)')
savePlot('./figures/DepthofThermocline.tif',type='tiff')
graphics.off()
#Can I do it for the whole grid?
#Will possibly crash...
#What I can do is define the areas used by Farley et al., 2017 (SC doc on project 35).
#See figure 17 and table 6 of that document.
#I will roughly define the area with samples.

windows(14,7)
plot(1,1,'n',xlim=c(110,310),ylim=c(-40,20),xlab='Lon',ylab='Lat')
map(database='world2',add=T,fill=T,col='black')
rect(120,0,140,10)
rect(160,5,180,15)
rect(160,-10,180,5)
rect(170,-25,185,-15)
rect(205,-20,230,5)
rect(250,-10,270,0)

text(c(130,170,170,177.5,217.5,260),c(5,10,-2.5,-20,-7,-5),labels=c('A','B','C.1','C.2','D','E'))
savePlot('./figures/AreasSimulation.jpg',type='jpg')

area<-matrix(rep(NA,length(depth20$lon)*length(depth20$lat)),ncol=length(depth20$lat))
area[depth20$lon>120 & depth20$lon<140,depth20$lat>0 & depth20$lat<10]<-'A'
area[depth20$lon>160 & depth20$lon<180,depth20$lat>5 & depth20$lat<15]<-'B'
area[depth20$lon>160 & depth20$lon<180,depth20$lat<5 & depth20$lat>-10]<-'C.1'
area[depth20$lon>170 & depth20$lon<185,depth20$lat< -15 & depth20$lat> -25]<-'C.2'
area[depth20$lon>210 & depth20$lon<230,depth20$lat<5 & depth20$lat>-20]<-'D'
area[depth20$lon>250 & depth20$lon<270,depth20$lat< 0 & depth20$lat> -10]<-'E'
#Calculate the median d20 and d18 values
d20vals<-tapply(depth20$val,area,mean,na.rm=T)
d18vals<-tapply(depth18$val,area,mean,na.rm=T)

newdata=data.frame(depth20=rep(d20vals,each=length(dat$length)),
                   dd.20.18=rep(d18vals,each=length(dat$length))-rep(d20vals,each=length(dat$length)),
                   len=rep(dat$length,times=length(d20vals)))

predicteddaychar<-predict(model$gam,newdata=newdata,type='response',se.fit=T)
save(predicteddaychar,file='predicteddepths.rdata')


predicteddaychar=data.frame(mean=predicteddaychar$fit,sd=predicteddaychar$se.fit)
#Include some variability...
predicteddaychar=apply(predicteddaychar,1,function(x){rnorm(1,mean=x[1],sd=x[2])})
predicteddaychar=data.frame(area=rep(names(d20vals),each=length(dat$length)),
                            age=rep(dat$age,times=length(d20vals)),
                            length=rep(dat$length,times=length(d20vals)),
                            depth=predicteddaychar^3)

#If I want to see the depth distribution by area
windows(14,10)
layout(matrix(1:6,ncol=3))
for (i in unique(predicteddaychar$area)[1:6]){
  hist(predicteddaychar$depth[predicteddaychar$area==i],main=i,ylab='Freq',xlab='Depth',100,xlim=c(100,500))
}
savePlot('./figures/FishDepthDist.jpg',type='jpg')
################################################
###### longline gear
##############################################
#Create a catenary to simulate a LL fishing gear depth.
#Following Bigelow et al...

hdepth<-function(HBF,phi=60/360*2*pi){
  ha=26.3
  hb=19.4
  L=50*(HBF+1)/2
  N=HBF+1
  # #ha length of brach line
  # hb length of float line
  # L is half of the length of the mainline between two floats
  # phi is the angle between the horizontal and tangential line of the mainline.
  sapply(1:HBF,function(x){ha+hb+L*((1+(1/tan(phi))^2)^0.5-((1-2*x/N)^2+(1/tan(phi))^2)^0.5)})
}

hdepth(5)
set.seed(1)
#If I want to include between and within set variability
#between set: by randomly generating 1000 values of phi with mean 60 and sd 11.3
#Within set variability was generated by generating 500 random samples of hook depth from
#from normal distributions with mean Dj and sd given by:
#sd(Dj)=8.73+4.4j
hbf<-15
phivals<-rnorm(100,mean=60,sd=11.3)
# phivals<-rnorm(100,mean=60,sd=1.3) #i get too deep values!
ll<-sapply(phivals,function(x){hdepth(hbf,phi=x)})
#res has two dim, the first one is the hook number, and the second one is the 
#between set variability. Now, I generate in a third dimension the inter-set variability
dim(ll)
d05<-apply(ll,2,function(x){dfun<-splinefun(1:hbf,x);max(dfun(seq(1,hbf,.1)))})

# plot(1:hbf,ll[,1])
# abline(h=d05[1],col='red')

ll2=array(data=NA,dim=c(hbf,50,100))
for (i in 1:hbf){
  ii=ifelse(i<(hbf/2),i,(hbf+1)-i) #Before, I used the hook no, but did not take into account hook
  #hbf is as close to the float as hook 1
  ll2[i,,]<-sapply(ll[i,],function(x){rnorm(50,mean=x,sd=8.73+4.4*ii)})
}
# hist(unlist(ll2))
#ll2 has three dims: hook number, within set variability, between set variability

#Now, include the effect of current.
surfacecur<-surfacecurrent(2007,3,depth=60)

plotimage(surfacecur$lon,surfacecur$lat,surfacecur$val,main='Surface current speed (cm/s)',xlab='Longitude',ylab='Latitude')
savePlot('./figures/cacaSurfaceCurrents.tif',type='tiff')
graphics.off()

area<-matrix(rep(NA,length(surfacecur$lon)*length(surfacecur$lat)),ncol=length(surfacecur$lat))
area[surfacecur$lon>120 & surfacecur$lon<140,surfacecur$lat>0 & surfacecur$lat<10]<-'A'
area[surfacecur$lon>160 & surfacecur$lon<180,surfacecur$lat>5 & surfacecur$lat<15]<-'B'
area[surfacecur$lon>160 & surfacecur$lon<180,surfacecur$lat<5 & surfacecur$lat>-10]<-'C.1'
area[surfacecur$lon>170 & surfacecur$lon<185,surfacecur$lat< -15 & surfacecur$lat> -25]<-'C.2'
area[surfacecur$lon>210 & surfacecur$lon<230,surfacecur$lat<5 & surfacecur$lat>-20]<-'D'
area[surfacecur$lon>250 & surfacecur$lon<270,surfacecur$lat< 0 & surfacecur$lat> -10]<-'E'
surfacecur<-tapply(surfacecur$val,area,mean,na.rm=T)

V=surfacecur #current speed in the surface layer
S=0.985*V
a=-0.025
#D2=D-D*(s/(1+exp(a*(D-D05))))

#little trial for the first hook
kk=ll2[1,,]*(S[1]/(1+exp(a*(ll2[1,,]-d05))))
ll[,1]-ll[,1]*(S[1]/(1+exp(a*(ll[,1]-d05[1]))))/100

#For the six areas, the fishind depth distribution would be
ll3=array(data=NA,dim=c(6,hbf,50,100))
for (area in 1:length(surfacecur)){
  for (hook in 1:hbf){
    ll3[area,hook,,]<-ll2[hook,,]-ll2[hook,,]*(S[area]/(1+exp(a*(ll2[hook,,]-d05))))/100
  }
}

#The fishing depth distribution accounted for current speed in the four areas would be
windows(14,10)
layout(matrix(1:6,ncol=3))
for (i in 1:6){
  hist(unlist(ll3[i,,,]),main=names(S)[i],ylab='Freq',xlab='depth',100,xlim=c(-5,600))
}
savePlot('./figures/cacaLLDist.jpg',type='jpg')
#Now, I have to fish all those fish....
#Each hook in the longline will catch the fish that is closer...
#I will fish with 5000 hooks. Each hook catches the closest fish.
#This takes in my computer like 6 minutes per iteration.

caught<-list() #for 5000 hooks taken randomly, this variable will store the index
                #of the fish caught.
areas<-names(S)
for (i in 1:length(areas)){
  print(paste(i,'out of',length(areas)))
ll4<-unlist(ll3[i,,,])[runif(5000,min=1,max=length(unlist(ll3[i,,,])))]
caught[[i]]<-sapply(ll4,function(x){ifelse(x<min(predicteddaychar$depth[predicteddaychar$area==areas[i]]) | x>max(predicteddaychar$depth[predicteddaychar$area==areas[i]]),NA,
                                           which.min(abs(x-predicteddaychar$depth[predicteddaychar$area==areas[i]])))})
}
save(caught,file='cacacaught.rdata')

#In each area, the fish caught would be
caught2<-list()
for (i in 1:length(areas)){
  caught2[[i]]<-predicteddaychar[predicteddaychar$area==areas[i],][caught[[i]],]
}
caught2<-do.call(rbind,caught2)
caught2<-caught2[!is.na(caught2$area),]
table(caught2$area) #The initial value was 5000. However, many hooks are not fishing 
                    #because they are out of the theoretical range of the species.
                    #The catenary must be improved.... See two lines below.

range(predicteddaychar$depth)
range(unlist(ll3))


head(sort(table(caught[[1]]),decreasing=T)) #Some hooks catch too many fish. The problem is that 
                                            #if I eliminate the duplicates I only retain the age 1.
                                            #Maybe I have to distribute randomly the rows.... Anyway,
                                            #let us assume we are using many longlines and that it is not an issue.


#Now I estimate the growth curve from 500 fish taken randomly in each area

estimates<-data.frame(area=NULL,Sinf=NULL,K=NULL,t0=NULL)
caughtsample<-list()
for (i in 1:length(areas)){

caughtsample[[i]]<-caught2[caught2$area==areas[i],][sample(nrow(caught2[caught2$area==areas[i],]),500),]
growth<-growth(intype=1,unit=1,size=caughtsample[[i]]$length,age=caughtsample[[i]]$age,Sinf=170,K=0.268,t0=-1)
estimates<-rbind(estimates,data.frame(area=areas[i],data.frame(t(growth$vout$m$getPars()))))
}
estimates

#
windows(12,10)
plot(1,1,'n', main="Growth by area", xlab = 'Age (quarters)', ylab = 'Length (cm)',xlim=c(0,40),ylim=c(0,200))
L2=NULL
for (i in 1:length(areas)){
points(caughtsample[[i]]$age,caughtsample[[i]]$length,pch=i,col=rainbow(length(areas))[i],cex=.5)
lines(seq(1,40,.1), estimates[i,2]*(1 - exp(-estimates[i,3]*(seq(1,40,.1)-estimates[i,4]))),col=rainbow(length(areas))[i],lwd=3)
L2<-c(L2,estimates[i,2]*(1 - exp(-estimates[i,3]*(40-estimates[i,4]))))
}
legend ('bottomright',pch=1:length(areas),col=rainbow(length(areas)),legend=areas)
savePlot('./figures/cacaVBfits.jpg',type='jpg')
estimates=cbind(estimates,L2)
estimates
names(estimates)[2]<-'Linf'
write.csv(estimates,file='cacaVBestimates.csv',row.names=F)
