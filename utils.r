#FA 2018/04/11
#A couple of functions to extract depth of the 20 and 18 degrees isotherm
#and the current speed from GODAS nc files

#Function to extract the 20 and 18 deg iso...
depthattemp<-function(year,month,temp){
  GODASdir<-'./godas data/'
  # GODASdir<-'C:/Users/abascal/Documents/tempdata/pottmp_files/'
  file=open.nc(paste(GODASdir,'pottmp.',year,'.nc',sep=''))
  #print.nc(file)
  
  depths<-var.get.nc(file,'level')
  mm<-var.get.nc(file,'time')
  lon<-var.get.nc(file,'lon')
  lat<-var.get.nc(file,'lat')
  
  lon.in=which(lon>120 & lon<280)
  lat.in=which(lat>-20 & lat<15)
  depths.in<-which(depths<1000)
  
  vals<-var.get.nc(file,'pottmp',start=c(lon.in[1],lat.in[1],depths.in[1],month),
                   count=c(length(lon.in),length(lat.in),length(depths.in),1))
  scale_factor<-att.get.nc(file,'pottmp','scale_factor')
  offset<-att.get.nc(file,'pottmp','add_offset')
  vals<-vals*scale_factor+offset-273
  close.nc(file)
  
  res<-apply(vals,1:2,function(x){
    ifelse(length(x[!is.na(x)])<2 | min(x,na.rm=T)>temp | max(x,na.rm=T)<temp,NA,
           approx(x,y=depths[depths.in],xout=temp)$y)}) 
  output=list(lon=lon[lon.in],lat=lat[lat.in],val=res)
  output
  
}

#Function to extract current in the surface layer
surfacecurrent<-function(year,month,depth=60){
  GODASdir<-'./godas data/'
  print(paste(GODASdir,'ucur.',year,'.nc',sep=''))
  ufile=open.nc(paste(GODASdir,'ucur.',year,'.nc',sep=''))
  
  depths<-var.get.nc(ufile,'level')
  mm<-var.get.nc(ufile,'time')
  lon<-var.get.nc(ufile,'lon')
  lat<-var.get.nc(ufile,'lat')
  
  lon.in=which(lon>120 & lon<280)
  lat.in=which(lat>-20 & lat<15)
  depths.in<-which(depths<depth)
  
  cat('Getting data from the first',depths[depths.in[length(depths.in)]],'m')
  
  #Read u file
  ucur<-var.get.nc(ufile,'ucur',start=c(lon.in[1],lat.in[1],depths.in[1],month),
                   count=c(length(lon.in),length(lat.in),length(depths.in),1))
  scale_factor<-att.get.nc(ufile,'ucur','scale_factor')
  ucur<-ucur*scale_factor
  close.nc(ufile)
  
  #Read v file
  vfile=open.nc(paste(GODASdir,'vcur.',year,'.nc',sep=''))
  vcur<-var.get.nc(ufile,'vcur',start=c(lon.in[1],lat.in[1],depths.in[1],month),
                   count=c(length(lon.in),length(lat.in),length(depths.in),1))
  scale_factor<-att.get.nc(vfile,'vcur','scale_factor')
  vcur<-vcur*scale_factor
  close.nc(vfile)
  # 
  #Calculate the velocity at each point
  cur<-sqrt(ucur^2+vcur^2)
  
  # Little check
  # cur[100,100,1]
  # sqrt(ucur[100,100,1]^2+vcur[100,100,1]^2)
  #OK
  
  #Average vertically
  curmean<-apply(cur,1:2,mean,na.rm=T)
  
  #Convert to cm/s
  curmean<-curmean*100
  # curmean[100,100]
  # mean(cur[100,100,])
  output=list(lon=lon[lon.in],lat=lat[lat.in],val=curmean)
  output
  
}

#Function to plot temp and currents
plotimage<-function(lon,lat,vals,main='',xlab='Longitude',ylab='Latitude'){
  windows(17,10)
  packages=c('akima','maps')
  sapply(packages,function(x) {if (!x %in% installed.packages()) install.packages(x)}) 
  lapply(packages, require, character.only=T)
  
  pal <- colorRampPalette(rev(c("red","yellow","#8BFFFF","#3EF0F5","#7D9DEC","#2F3AF8","purple")))
  
  windows(17,10)
  layout(matrix(2:1, nrow=1),widths=c(8,1.5))
  par(mai=c(1,0,1,1.5))
  rangevar<-range(vals,na.rm=T)
  
  zbreaks <- seq(rangevar[1],rangevar[2],diff(rangevar)/100)
  cols <-pal(length(zbreaks)-1)
  image(x=.01, y=zbreaks, z=matrix(zbreaks,1), col=cols, breaks=zbreaks, useRaster=TRUE, xlab="", ylab="", axes=FALSE)
  axis(4, at=pretty(zbreaks,5), las=2)
  
  par(mai=c(1.02,0.82,0.82,0.42))
  
  image(lon,lat,vals, col=cols, breaks=zbreaks, xlab=xlab,ylab=ylab,main=main)
  
  map(add=T,col='black',fill=T)
  box()
  
}